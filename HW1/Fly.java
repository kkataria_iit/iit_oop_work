
public class Fly extends Creature implements Flyer{

	public Fly(String name) {
		super(name);
	}

	@Override
	public void move() {
	 System.out.println( this + "is hovering");	
	}

	@Override
	public void fly() {
		System.out.println( this + " is buzzing around in flight.");	
	}
	
	@Override
	public void eat(Thing aThing)
	{
		if (aThing instanceof Creature)
		{
			System.out.println( this + " will not eat a " + aThing );
		}
		else
		{
			stomach = aThing;
			System.out.println( this + " has just eaten a " + stomach );
		}
	}
}
