

public class TestCreature {

	public static final int THING_COUNT = 5;
	public static final int CREATURE_COUNT = 4;
	
	public static void main( String[] args)
	{
		Thing[] things = new Thing[THING_COUNT];
		Creature[] creatures = new Creature[CREATURE_COUNT];
		
		things[0] = new Thing( "Banana" );
		things[1] = new Tiger( "Tiger, Pooh's Friend" );
		things[2] = new Thing( "Locomotive" );
		things[3] = new Thing( "Tick-Tock the Crocodile");
		things[4] = new Thing( "The Great Lion");
		
		creatures[0] = new Ant("Fire");
		creatures[1] = new Bat("Natalus");
		creatures[2] = new Fly("Bot");
		creatures[3] = new Tiger("Syberian");
		
		System.out.println("Things:\n");
		for(int i = 0;i<things.length;i++)
			System.out.println(things[i]);
		
		System.out.println("\nCreatures:\n");
		for(int i = 0;i<creatures.length;i++)
			System.out.println(creatures[i]);
		
		System.out.println("\nTest Tiger\n");
		Creature tiger = new Tiger("Bengal");
		tiger.move();
		tiger.eat(creatures[1]);
		tiger.whatDidYouEat();
		
		System.out.println("\nTest Ant\n");
		Creature redAnt = new Ant("Red");
		redAnt.move();
		redAnt.eat(new Thing("Sugar"));
		redAnt.whatDidYouEat();
		
		Creature blackAnt = new Ant("Black");
		blackAnt.whatDidYouEat();
		
		System.out.println("\nTest Bat\n");
		Creature bat = new Bat("Bulldog");
		bat.move();
		bat.eat(new Fly("Horse"));
		bat.whatDidYouEat();
		bat.eat(new Thing("Banana"));
		bat.whatDidYouEat();
		
		System.out.println("\nTest Fly\n");
		Creature fly = new Fly("Horse");
		fly.move();
		fly.eat(new Ant("Fire"));
		fly.whatDidYouEat();
		fly.eat(things[0]);
		fly.whatDidYouEat();
		
		
		
	}
}
