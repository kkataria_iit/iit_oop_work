
public class Bat extends Creature implements Flyer{

	public Bat(String name) {
		super(name);
	}
	
	@Override
	public void move() {
		System.out.println( this + " is flying");
	}

	@Override
	public void fly() {
		System.out.println( this + " is swooping through the dark.");
	}
	
	@Override
	public void eat(Thing aThing)
	{
		if (!(aThing instanceof Creature))
		{
			System.out.println( this + " will not eat a " + aThing );
		}
		else
		{
			stomach = aThing;
			System.out.println( this + " has just eaten a " + stomach );
		}
	}

}
