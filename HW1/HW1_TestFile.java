import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HW1_TestFile {
	
	Thing aThing = new Thing("Pencil");
	String str = "Pencil Thing";
	Creature tiger = new Tiger("Syberian");
	String tigerStr = "Syberian Tiger";
	Creature ant = new Ant("Red");
	String antStr = "Red Ant";
	Creature bat = new Bat("Bulldog");
	String batStr = "Bulldog Bat";
	Creature fly = new Fly("Horse");
	String flyStr = "Horse Fly";
	
	@Test
	public void testCreateAndPrintThing()
	{
		
		assertEquals(aThing.toString(), str);
	}
	
	@Test
	public void testCreateAndPrintCreatures()
	{
		assertEquals(tiger.toString(), tigerStr);
		assertEquals(ant.toString(), antStr);
		assertEquals(bat.toString(), batStr);
		assertEquals(fly.toString(), flyStr);
	}
}
