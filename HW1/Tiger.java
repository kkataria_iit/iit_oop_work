
public class Tiger extends Creature{

	public Tiger(String name) {
		super(name);
	}
	
	public void move()
	{
		System.out.printf( this + " has just pounced.\n");
	}

}
