

public abstract class Creature extends Thing{

	public Thing stomach;
	
	public Creature(String name) {
		super(name);
	}
	
	public abstract void move();
	
	public void eat(Thing aThing)
	{
		stomach = aThing;
		System.out.println( this + " has just eaten a " +  aThing);
	}
	
	public void whatDidYouEat()
	{
		if( stomach == null )
			System.out.println( this + " has had nothing to eat!");
		else
			System.out.println(this + " has eaten " + stomach);
	}

}
