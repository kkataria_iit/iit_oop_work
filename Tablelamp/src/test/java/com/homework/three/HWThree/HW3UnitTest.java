package com.homework.three.HWThree;

import com.homework.three.button.PushdownButton;
import com.homework.three.lightbulb.Lightbulb;
import com.homework.three.main.App;
import com.homework.three.main.Switchable;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.TestCase;

public class HW3UnitTest 
    extends TestCase
{
	PushdownButton pb;
	Switchable appliance = new Lightbulb();
	
	public HW3UnitTest( String testName )
    {
		super( testName );
		pb = new PushdownButton(appliance);
    }

	@Test
	public void test_main_program() {
		App.main(null);
	}
	
	@Test
	public void test_pushdown_btn_on_off_default() {
		assertEquals(pb.btn_on_off, false);
	}
	
	@Test
	public void test_pushdown_btn_on_off_on_one_push() {
		pb = new PushdownButton(appliance);
		pb.PushButton();
		assertEquals(pb.btn_on_off, true);
	}
	
	@Test
	public void test_pushdown_btn_on_off_on_two_push() {
		pb = new PushdownButton(appliance);
		pb.PushButton();
		pb.PushButton();
		assertEquals(pb.btn_on_off, false);
	}
	
	@Test
	public void test_pushdown_btn_on_off_on_odd_push() {
		pb = new PushdownButton(appliance);
		for(int i = 0; i<5;i++)
		{
			pb.PushButton();
		}
		assertEquals(pb.btn_on_off, true);
	}
	
	@Test
	public void test_pushdown_btn_on_off_on_even_push() {
		pb = new PushdownButton(appliance);
		for(int i =0; i<6;i++) {
			pb.PushButton();
		}
		assertEquals(pb.btn_on_off, false);
	}
}
