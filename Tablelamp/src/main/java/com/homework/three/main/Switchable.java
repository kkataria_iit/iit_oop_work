package com.homework.three.main;

public interface Switchable {
	
	public void on();
	public void off();

}
