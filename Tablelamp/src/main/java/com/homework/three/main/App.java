package com.homework.three.main;

import com.homework.three.button.Button;
import com.homework.three.button.PushdownButton;
import com.homework.three.lightbulb.Lightbulb;

public class App 
{
    public static void main( String[] args )
    {
    	Button b = new Button();
    	b.switchOff();
    	b.switchOn();
    	
    	Switchable lightbulb = new Lightbulb();
    	
    	Switches pushbtn = new PushdownButton(lightbulb);
    	
    	Tablelamp tl = new Tablelamp(pushbtn);
    	for(int i = 0; i  < 4 ;i++)
    		tl.command();
    }
}
