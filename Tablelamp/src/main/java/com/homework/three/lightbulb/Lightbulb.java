package com.homework.three.lightbulb;

import com.homework.three.main.Switchable;

public class Lightbulb implements Switchable{

	@Override
	public void on() {
		System.out.println("Lightbulb on");
	}
	
	@Override
	public void off() {
		System.out.println("Lightbulb off");
	}
}
