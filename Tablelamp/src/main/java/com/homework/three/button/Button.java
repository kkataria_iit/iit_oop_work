package com.homework.three.button;

import com.homework.three.lightbulb.Lightbulb;

public class Button {
	Lightbulb lb;
	
	public Button()
	{
		lb = new Lightbulb();
	}
	
	public void switchOn() {
		System.out.println("Button switched to ON");
		lb.on();
	}
	
	public void switchOff() {
		System.out.println("Button switched to OFF");
		lb.off();
	}
}
