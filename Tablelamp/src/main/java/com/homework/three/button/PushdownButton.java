package com.homework.three.button;

import com.homework.three.main.Switchable;
import com.homework.three.main.Switches;

public class PushdownButton implements Switches{
	
	public boolean btn_on_off;
	public Switchable appliance;
	
	public PushdownButton(Switchable appliance) {
		this.btn_on_off = false;
		this.appliance = appliance;
	}
	
	@Override
	public boolean isOn() {
        return this.btn_on_off;
    }
	
	@Override
	public void PushButton(){
	
		if( this.isOn() )
		{
			this.btn_on_off = false;
			System.out.println("Button switched to OFF");
			appliance.off();
		}
		else
		{	
			this.btn_on_off = true;
			System.out.println("Button switched to ON");
			appliance.on();
		}	
	}
}
