package com.homework.two;

public class D {
	
	int d_number;
	B b;
	F f[] = new F[5];
	public D() {
		System.out.println("D Constructor");
		this.d_number = 0;
	}
	
	public D(int d) {
		System.out.println("D Constructor");
		this.d_number = d;
	}
	
	public D(C c) {
		System.out.println("D Constructor");
		this.d_number = c.getNumber();
	}
	
	public D(B b1) {
		System.out.println("D Constructor");
		b = b1;
	}
}
