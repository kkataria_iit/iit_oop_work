package com.homework.two;

public class A {
	
	public int a_number;
	
	public A() {
		System.out.println("A Constructor");
		this.a_number = 0;
	}
	
	public A(int a) {
		System.out.println("A Constructor");
		this.a_number = a;
	}

	public int getNumber() {
		return a_number;
	}
}
