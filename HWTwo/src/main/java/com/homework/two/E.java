package com.homework.two;

public class E extends C{
	
public int e_number;
	
	public E() {
		super();
		System.out.println("E Constructor");
		this.e_number = 0;
	}
	
	public E(int e) {
		super(e);
		System.out.println("E Constructor");
		this.e_number = e;
	}

}
