package com.homework.two;

public class C extends A{
	
	public int c_number;
	
	public C() {
		super();
		System.out.println("C Constructor");
		c_number = 0;
	}
	
	public C(int c) {
		super(c);
		System.out.println("C Constructor");
		c_number = c;

	}

}
