package com.homework.two;

public class MainTestProgram {
	
	public static void main(String []args) {
		
		ImprovedRandom ir1;
		ImprovedStringTokenizer ist1 = new ImprovedStringTokenizer("Hello World");
		ImprovedStringTokenizer ist2 = new ImprovedStringTokenizer("Hello/World","/");
		ImprovedStringTokenizer ist3 = new ImprovedStringTokenizer("Hello/World","/",true);
		IntegerToHex ith = new IntegerToHex();
		
		ir1 = new ImprovedRandom();
		
		System.out.println("Random Integer within Range : " + ir1.nextInt(10, 20));
		System.out.println("Random Integer within Range again: " + ir1.nextInt(10, 20));

		System.out.println("Actual String : \"Hello World\"");
		System.out.println("All Words without delims:");
		String[] s1 = ist1.allTokens();
		for( String str : s1)  
        { 
			System.out.println(str);
        } 
		System.out.println("Actual String : \"Hello/World\"");
		System.out.println("All Words with delims as separator:");
		s1 = ist2.allTokens("/");
		for( String str : s1)  
        { 
			System.out.println(str);
        } 
		System.out.println("Actual String : \"Hello/World\"");
		System.out.println("All Words with delims as a string:");
		s1 = ist3.allTokens("/");
		for( String str : s1)  
        { 
			System.out.println(str);
        }
		
		int x = 1728;
		System.out.println("Hex Value of 1728 is : " + ith.getHexValue(x));
		
		B b = new B(10);
		System.out.println("B : " + b.b_number);
		A a = new A(12);
		System.out.println("A : " + a.a_number);
		b = new B(a);
		System.out.println("B : " + b.b_number);
		D d = new D();
		System.out.println("D : " + d.d_number);
		C c = new C(15);
		System.out.println("C : " + c.c_number);
		d = new D(c);
		System.out.println("D : " + d.d_number);
		d = new D(b);
		System.out.println("D : " + d.d_number);
		C e = new E(10);
		System.out.println("E->C : " + e.c_number);
		F f = new F(10);
		System.out.println("F : " + f.f_number);
	}

}
