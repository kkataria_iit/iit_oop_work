package com.homework.two;

import java.util.Random;

public class ImprovedRandom extends Random{
	
	public ImprovedRandom() {
		super();		
	}
	
	public ImprovedRandom(long seed) {
		super(seed);
	}
	
	public int nextInt(int lowerLimit, int upperLimit){
		if (lowerLimit > upperLimit) {
			throw new IllegalArgumentException("Lower Limit must be less than or equal to Upper Limit");
		}
		return(this.nextInt((upperLimit - lowerLimit) + 1) + lowerLimit);		
	}
	

}
