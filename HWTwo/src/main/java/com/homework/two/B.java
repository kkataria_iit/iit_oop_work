package com.homework.two;

public class B {
	
	int b_number;
	
	public B() {
		System.out.println("B Constructor");
		this.b_number = 0;
	}
	
	public B(int b) {
		System.out.println("B Constructor");
		this.b_number = b;
	}
	
	public B(A a) {
		System.out.println("B Constructor");
		this.b_number = a.getNumber();
	}
}
