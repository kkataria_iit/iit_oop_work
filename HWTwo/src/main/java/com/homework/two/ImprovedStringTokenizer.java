package com.homework.two;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class ImprovedStringTokenizer extends StringTokenizer {

	public ImprovedStringTokenizer(String str) {
		super(str);
	}
	
	public ImprovedStringTokenizer(String str, String delim) {
		super(str, delim);
	}
	
	public ImprovedStringTokenizer(String str, String delim, boolean returnDelims) {
		super(str, delim, returnDelims);
	}
	
	public String[] allTokens() {
		String[] tokens = new String[this.countTokens()];
		int i=0;
		while(this.hasMoreTokens()) {
			tokens[i] = this.nextToken();
			i++;
		}
		return (tokens);
	}
	
	public String[] allTokens(String delims) {				
		ArrayList<String> list = new ArrayList<String>();

		while (this.hasMoreTokens()) {
			list.add( this.nextToken("/") );
	      }
		String[] tokens = new String[ list.size() ];

		for(int j = 0; j < tokens.length; j++ )
			tokens[ j ] = list.get( j ).toString();
		
		return(tokens);
	}
}
