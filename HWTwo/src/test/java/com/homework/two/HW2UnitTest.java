package com.homework.two;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.TestCase;

public class HW2UnitTest extends TestCase{
	
	ImprovedRandom ir1,ir2;
	ImprovedStringTokenizer ist1 = new ImprovedStringTokenizer("Hello World");
	ImprovedStringTokenizer ist2 = new ImprovedStringTokenizer("Hello/World","/");
	ImprovedStringTokenizer ist3 = new ImprovedStringTokenizer("a/b","/",true);
	IntegerToHex ith = new IntegerToHex();
	
	public HW2UnitTest( String testName )
    {
        super( testName );
    }
	
	@Test
	public void test_main_program() {
		MainTestProgram mtp = new MainTestProgram();
		MainTestProgram.main(null);
	}
	
	@Test
	public void test_ImprovedRandom_range() {
		ir1 = new ImprovedRandom(10);
		ir2 = new ImprovedRandom(10);
		
		assertEquals(ir1.nextInt(10, 20),ir2.nextInt(10,20));
	}
	
	@Test
	public void test_ImprovedRandom_range_exception() {
		
		ir1 = new ImprovedRandom();		
		try {
			ir1.nextInt(20,10);
	    } catch(IllegalArgumentException e) {
	    	assertEquals("Lower Limit must be less than or equal to Upper Limit",e.getMessage());
	    }        
	}
	
	@Test
	public void test_ImprovedStringTokenizer_without_delims() {
		
		String[] s1 = ist1.allTokens();
		String[] s2 = new String[2];
		s2[0] = "Hello";
		s2[1] = "World";
		assertArrayEquals(s1,s2);
	}
	
	@Test
	public void test_ImprovedStringTokenizer_with_delims() {
		
		String[] s1 = ist2.allTokens("/");
		String[] s2 = new String[2];
		s2[0] = "Hello";
		s2[1] = "World";
		assertArrayEquals(s1,s2);
	}
	@Test
	public void test_ImprovedStringTokenizer_with_delims_true() {
		
		String[] s1 = ist3.allTokens("/");
		String[] s2 = new String[3];
		s2[0] = "a";
		s2[1] = "/";
		s2[2] = "b";
		assertArrayEquals(s1,s2);
	}
	
	@Test
	public void test_IntegerToHex() {
		int x = 1728;
		assertEquals("6c0", ith.getHexValue(x));
	}
	
	@Test
	public void test_B_empty_constructor() {
		B b = new B();
		assertEquals(0,b.b_number);
	}
	
	@Test
	public void test_B_non_empty_constructor() {
		B b1 = new B(10);
		assertEquals(10,b1.b_number);
		
		A a = new A();
		b1 = new B(a);
		assertEquals(0, b1.b_number);
		
		a = new A(12);
		b1 = new B(a);
		assertEquals(12, b1.b_number);
	}
	
	@Test
	public void test_D_empty_constructor() {
		D d = new D();
		assertEquals(0,d.d_number);
	}
	
	@Test
	public void test_D_non_empty_constructor() {
		D d1 = new D(10);
		assertEquals(10,d1.d_number);
		
		C c = new C();
		d1 = new D(c);
		assertEquals(0, d1.d_number);
		
		c = new C(15);
		d1 = new D(c);
		assertEquals(15, d1.d_number);
		
		B b = new B(10);
		d1 = new D(b);
		assertEquals(b,d1.b);
	}
	
	@Test
	public void test_E_empty_constructor() {
		E e = new E();
		assertEquals(0,e.e_number);
	}
	
	@Test
	public void test_E_non_empty_constructor() {
		E e1 = new E(10);
		assertEquals(10,e1.e_number);
		
		C e2 = new E(10);
		assertEquals(10,e2.c_number);
	}
	
	@Test
	public void test_F_empty_constructor() {
		F f = new F();
		assertEquals(0,f.f_number);
	}
	
	@Test
	public void test_F_non_empty_constructor() {
		F f1 = new F(10);
		assertEquals(10,f1.f_number);
	}
}
